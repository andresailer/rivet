BEGIN PLOT /KEDR_2022_I2593308/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $J/\psi\to \pi^+\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /KEDR_2022_I2593308/d01-x01-y02
Title=$\pi^+\pi^0$ mass distribution in $J/\psi\to \pi^+\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /KEDR_2022_I2593308/d01-x01-y03
Title=$\pi^-\pi^0$ mass distribution in $J/\psi\to \pi^+\pi^-\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
