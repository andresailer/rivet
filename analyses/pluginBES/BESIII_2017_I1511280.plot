BEGIN PLOT /BESIII_2017_I1511280/d01-x01-y01
Title=$K^-\pi^+_1$ mass distribution in $D^0\to K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^-\pi^+_1}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+_1}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1511280/d01-x01-y02
Title=$K^-\pi^+_2$ mass distribution in $D^0\to K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^-\pi^+_2}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+_2}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1511280/d01-x01-y03
Title=$\pi^+_1\pi^-$ mass distribution in $D^0\to K^-\pi^+\pi^+\pi^-$
XLabel=$m_{\pi^+_1\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+_1\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1511280/d01-x01-y04
Title=$\pi^+_2\pi^-$ mass distribution in $D^0\to K^-\pi^+\pi^+\pi^-$
XLabel=$m_{\pi^+_2\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+_2\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1511280/d01-x01-y05
Title=$K^-\pi^+_1\pi^-$ mass distribution in $D^0\to K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^-\pi^+_1\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+_1\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1511280/d01-x01-y06
Title=$K^-\pi^+_2\pi^-$ mass distribution in $D^0\to K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^-\pi^+_2\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+_2\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1511280/d01-x01-y07
Title=$\pi^+\pi^+\pi^-$ mass distribution in $D^0\to K^-\pi^+\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1511280/d01-x01-y08
Title=$K^-\pi^+\pi^+$ mass distribution in $D^0\to K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^-\pi^+\pi^+}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+\pi^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
