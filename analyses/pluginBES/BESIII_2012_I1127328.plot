BEGIN PLOT /BESIII_2012_I1127328/d01-x01-y01
Title=$K^+K^-$ mass distribution in $\psi(2S)\to K^+K^-\pi^0$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2012_I1127328/d01-x01-y02
Title=$K^\pm\pi^0$ mass distribution in $\psi(2S)\to K^+K^-\pi^0$
XLabel=$m_{K^\pm\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^\pm\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2012_I1127328/dalitz
Title=Dalitz plot for  $\psi(2S)\to K^+K^-\pi^0$
XLabel=$m^2_{K^+\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^-\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^+\pi^0}/{\rm d}m^2_{K^-\pi^0}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
