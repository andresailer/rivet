Name: BESIII_2021_I1940222
Year: 2021
Summary: Mass distributions in the decays $D^0\to K^-\pi^+\omega$, $D^0\to K^0_S\pi^0\omega$, $D^+\to K^0_S\pi^+\omega$
Experiment: BESIII
Collider: BEPC
InspireID: 1940222
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 105 (2022) 3, 032009
RunInfo: Any process producing D0 or D+ mesons
Description:
  'Measurement of the mass distributions in the decays $D^0\to K^-\pi^+\omega$, $D^0\to K^0_S\pi^0\omega$, $D^+\to K^0_S\pi^+\omega$ by BES. The data were read from the plots in the paper and therefore for some points the error bars are the size of the point. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021zma
BibTeX: '@article{BESIII:2021zma,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Observation of~$D^{0(+)}\to K^0_S\pi^{0(+)}\omega $ and improved measurement of $D^0\to K^-\pi^+\omega$}",
    eprint = "2110.03415",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.105.032009",
    journal = "Phys. Rev. D",
    volume = "105",
    number = "3",
    pages = "032009",
    year = "2022"
}
'
