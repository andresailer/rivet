BEGIN PLOT /BESIII_2021_I1929314/d01-x01-y01
Title=$\sigma(e^+e^-\to K^+K^-\pi^+\pi^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to K^+K^-\pi^+\pi^-)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2021_I1929314/d01-x01-y02
Title=$\sigma(e^+e^-\to K^+K^-K^+K^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to K^+K^-K^+K^-)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2021_I1929314/d01-x01-y03
Title=$\sigma(e^+e^-\to \pi^+\pi^-\pi^+\pi^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \pi^+\pi^-\pi^+\pi^-)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2021_I1929314/d01-x01-y04
Title=$\sigma(e^+e^-\to p\bar{p}\pi^+\pi^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to p\bar{p}\pi^+\pi^-)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2021_I1929314/d01-x01-y05
Title=$\sigma(e^+e^-\to K^+K^-\pi^+\pi^-\pi^0)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to K^+K^-\pi^+\pi^-\pi^0)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2021_I1929314/d01-x01-y06
Title=$\sigma(e^+e^-\to K^+K^-K^+K^-\pi^0)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to K^+K^-K^+K^-\pi^0)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2021_I1929314/d01-x01-y07
Title=$\sigma(e^+e^-\to \pi^+\pi^-\pi^+\pi^-\pi^0)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \pi^+\pi^-\pi^+\pi^-\pi^0)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2021_I1929314/d01-x01-y08
Title=$\sigma(e^+e^-\to p\bar{p}\pi^+\pi^-\pi^0)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to p\bar{p}\pi^+\pi^-\pi^0)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
