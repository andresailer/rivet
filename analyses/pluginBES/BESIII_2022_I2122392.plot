BEGIN PLOT /BESIII_2022_I2122392/d01-x01-y01
Title=$\Lambda\omega$ mass distribution in $\psi(2S)\to \Lambda\bar{\Lambda}\omega$
XLabel=$m^2_{\Lambda\omega}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda\omega}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2122392/d01-x01-y02
Title=$\bar{\Lambda}\omega$ mass distribution in $\psi(2S)\to \Lambda\bar{\Lambda}\omega$
XLabel=$m^2_{\bar{\Lambda}\omega}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{\Lambda}\omega}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2122392/dalitz
Title=Dalitz plot for  $\psi(2S)\to \Lambda\bar{\Lambda}\omega$
XLabel=$m^2_{\Lambda\omega}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{\Lambda}\omega}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\Lambda\omega}/{\rm d}m^2_{\bar{\Lambda}\omega}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
