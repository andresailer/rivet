BEGIN PLOT /BESIII_2022_I2135117/d01-x01-y01
Title=$\eta\eta^\prime$ mass distribution in $J/\psi\to\eta\eta^\prime$
XLabel=$m_{\eta\eta^\prime}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\eta\eta^\prime}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2135117/d01-x01-y02
Title=$\gamma\eta$ mass distribution in $J/\psi\to\eta\eta^\prime$
XLabel=$m_{\gamma\eta}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\gamma\eta}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2135117/d01-x01-y03
Title=$\gamma\eta^\prime$ mass distribution in $J/\psi\to\eta\eta^\prime$
XLabel=$m_{\gamma\eta^\prime}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\gamma\eta^\prime}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2135117/d01-x01-y04
Title=$\cos\theta_\eta$ distribution in $J/\psi\to\eta\eta^\prime$
XLabel=$\cos\theta_\eta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\eta$
LogY=0
END PLOT

BEGIN PLOT /BESIII_2022_I2135117/d02-x01-y01
Title=$\cos\theta_\eta$ distribution in $J/\psi\to\eta\eta^\prime$ ($1.5<m_{\eta\eta^\prime}<1.7$\,GeV)
XLabel=$\cos\theta_\eta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\eta$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2135117/d02-x01-y02
Title=$\cos\theta_\eta$ distribution in $J/\psi\to\eta\eta^\prime$ ($1.7<m_{\eta\eta^\prime}<2.0$\,GeV)
XLabel=$\cos\theta_\eta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\eta$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2135117/d02-x01-y03
Title=$\cos\theta_\eta$ distribution in $J/\psi\to\eta\eta^\prime$ ($2.0<m_{\eta\eta^\prime}<3.2$\,GeV)
XLabel=$\cos\theta_\eta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\eta$
LogY=0
END PLOT
