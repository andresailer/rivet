Name: BESIII_2021_I1837968
Year: 2021
Summary: Mass distributions in $\Lambda_c^+\to p K^0_S \eta$
Experiment: BESIII
Collider: BEPC
InspireID: 1837968
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Lett.B 817 (2021) 136327
RunInfo: Any process producing Lambda_c+ mesons
Description:
  'Measurement of the mass distributions in the decay $\Lambda_c^+\to p K^0_S \eta$ by BESIII. The data were read from the plots in the paper and may not have been corrected for efficiency/acceptance.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2020kzc
BibTeX: '@article{BESIII:2020kzc,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Measurement of the absolute branching fraction of $\Lambda_c^+\to p K^0_{\mathrm{S}}\eta$ decays}",
    eprint = "2012.11106",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1016/j.physletb.2021.136327",
    journal = "Phys. Lett. B",
    volume = "817",
    pages = "136327",
    year = "2021"
}
'
