
BEGIN PLOT /BESIII_2022_I2033855/
LogY=0
END PLOT

BEGIN PLOT /BESIII_2022_I2033855/d01-x01-y01
Title=$\alpha_\psi$ for $\chi_{c0}\to \Xi^-\bar\Xi^+$
YLabel=$\alpha_\psi$
END PLOT
BEGIN PLOT /BESIII_2022_I2033855/d01-x01-y02
Title=$\alpha_\psi$ for $\chi_{c0}\to \Xi^0\bar\Xi^0$
YLabel=$\alpha_\psi$
END PLOT
BEGIN PLOT /BESIII_2022_I2033855/d02-x01-y01
Title=$\alpha_\psi$ for $\chi_{c1}\to \Xi^-\bar\Xi^+$
YLabel=$\alpha_\psi$
END PLOT
BEGIN PLOT /BESIII_2022_I2033855/d02-x01-y02
Title=$\alpha_\psi$ for $\chi_{c1}\to \Xi^0\bar\Xi^0$
YLabel=$\alpha_\psi$
END PLOT
BEGIN PLOT /BESIII_2022_I2033855/d03-x01-y01
Title=$\alpha_\psi$ for $\chi_{c2}\to \Xi^-\bar\Xi^+$
YLabel=$\alpha_\psi$
END PLOT
BEGIN PLOT /BESIII_2022_I2033855/d03-x01-y02
Title=$\alpha_\psi$ for $\chi_{c2}\to \Xi^0\bar\Xi^0$
YLabel=$\alpha_\psi$
END PLOT

BEGIN PLOT /BESIII_2022_I2033855/d04-x01-y01
Title=Helicity angle for $\chi_{c0}\to \Xi^-\bar\Xi^+$
XLabel=$\cos\theta$
YLabel=$1\Gamma\text{d}\Gamma/\text{d}\cos\theta$
END PLOT
BEGIN PLOT /BESIII_2022_I2033855/d04-x01-y02
Title=Helicity angle for $\chi_{c0}\to \Xi^0\bar\Xi^0$
XLabel=$\cos\theta$
YLabel=$1\Gamma\text{d}\Gamma/\text{d}\cos\theta$
END PLOT
BEGIN PLOT /BESIII_2022_I2033855/d05-x01-y01
Title=Helicity angle for $\chi_{c1}\to \Xi^-\bar\Xi^+$
XLabel=$\cos\theta$
YLabel=$1\Gamma\text{d}\Gamma/\text{d}\cos\theta$
END PLOT
BEGIN PLOT /BESIII_2022_I2033855/d05-x01-y02
Title=Helicity angle for $\chi_{c1}\to \Xi^0\bar\Xi^0$
XLabel=$\cos\theta$
YLabel=$1\Gamma\text{d}\Gamma/\text{d}\cos\theta$
END PLOT
BEGIN PLOT /BESIII_2022_I2033855/d06-x01-y01
Title=Helicity angle for $\chi_{c2}\to \Xi^-\bar\Xi^+$
XLabel=$\cos\theta$
YLabel=$1\Gamma\text{d}\Gamma/\text{d}\cos\theta$
END PLOT
BEGIN PLOT /BESIII_2022_I2033855/d06-x01-y02
Title=Helicity angle for $\chi_{c2}\to \Xi^0\bar\Xi^0$
XLabel=$\cos\theta$
YLabel=$1\Gamma\text{d}\Gamma/\text{d}\cos\theta$
END PLOT
