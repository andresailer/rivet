BEGIN PLOT /BESIII_2020_I1823448/d01-x01-y04
Title=$\sigma(e^+e^-\to \Xi^-\bar{\Xi}^+)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \Xi^-\bar{\Xi}^+)$/pb
ConnectGaps=1
LogY=0
END PLOT
