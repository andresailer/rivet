BEGIN PLOT /L3_1992_I334954/d01-x01-y01
Title=Thrust, $T$
XLabel=$T$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{T}$
FullRange=1
END PLOT
BEGIN PLOT /L3_1992_I334954/d02-x01-y01
Title=Thrust major, $M$
XLabel=$M$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{M}$
FullRange=1
END PLOT
BEGIN PLOT /L3_1992_I334954/d03-x01-y01
Title=Thrust minor, $m$
XLabel=$m$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{m}$
FullRange=1
END PLOT
BEGIN PLOT /L3_1992_I334954/d04-x01-y01
Title=Oblateness, $M - m$
XLabel=$O$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{O}$
FullRange=1
END PLOT
BEGIN PLOT /L3_1992_I334954/d06-x01-y01
Title=Differential 2-Jet rate (Durham)
XLabel=$D_2$
YLabel=$1/\sigma \mathrm{d}\sigma/\mathrm{d}(d_2)$
END PLOT
BEGIN PLOT /L3_1992_I334954/d07-x01-y01
Title=Differential 2-Jet rate (JADE)
XLabel=$D_2$
YLabel=$1/\sigma \mathrm{d}\sigma/\mathrm{d}(d_2)$
END PLOT

BEGIN PLOT /L3_1992_I334954/d10-x01-y01
Title=Sphericity, $S$
XLabel=$S$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{S}$
FullRange=1
END PLOT
BEGIN PLOT /L3_1992_I334954/d11-x01-y01
Title=Aplanarity, $A$
XLabel=$A$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{A}$
FullRange=1
END PLOT
BEGIN PLOT /L3_1992_I334954/d12-x01-y01
Title=C parameter, $C$
XLabel=$C$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{C}$
FullRange=1
END PLOT
BEGIN PLOT /L3_1992_I334954/d13-x01-y01
Title=D parameter, $D$
XLabel=$D$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{D}$
FullRange=1
END PLOT
BEGIN PLOT /L3_1992_I334954/d14-x01-y01
Title=Heavy jet mass
XLabel=$M_h^2/s$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{(M_h^2/s)}$
FullRange=1
END PLOT
BEGIN PLOT /L3_1992_I334954/d15-x01-y01
Title=Light jet mass
XLabel=$M_l^2/s$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{(M_l^2/s)}$
FullRange=1
END PLOT
BEGIN PLOT /L3_1992_I334954/d16-x01-y01
Title=Charge Particle Multiplicity
XLabel=$N_{\text{charged}}$
YLabel=Probability/\%
END PLOT