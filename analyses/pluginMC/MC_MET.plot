# BEGIN PLOT /MC_MET/met_incl
Title=Vector missing $E_\mathrm{T}^\mathrm{miss}$ in full acceptance
XLabel=Vector missing $E_\mathrm{T}^\mathrm{miss}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}\sum E_\mathrm{T}^\mathrm{miss}$ [pb GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /MC_MET/set_incl
Title=Scalar $\sum E_\mathrm{T}^\mathrm{miss}$ in full acceptance
XLabel=Scalar $\sum E_\mathrm{T}^\mathrm{miss}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}\sum E_\mathrm{T}^\mathrm{miss}$ [pb GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /MC_MET/met_calo
Title=Vector missing $E_\mathrm{T}^{miss}$ $|\eta| < 5$ acceptance
XLabel=Vector missing $E_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} E_\mathrm{T}^{miss}$ [pb GeV$^{-1}$]
LogX=1
# END PLOT

# BEGIN PLOT /MC_MET/set_calo
Title=Scalar $\sum E_\mathrm{T}$ $|\eta| < 5$ acceptance
XLabel=Scalar $\sum E_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}\sum E_\mathrm{T}$ [pb GeV$^{-1}$]
LogX=1
# END PLOT

# BEGIN PLOT /MC_MET/pT_inv
Title=$p_\mathrm{T}$ of vector sum of all invisibles
XLabel=$p_\mathrm{T}^{inv}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_\mathrm{T}^{inv}$ [pb GeV$^{-1}$]
LogX=1
# END PLOT

# BEGIN PLOT /MC_MET/mass_inv
Title=Mass $m^{inv}$ of vector sum of all invisibles
XLabel=$m^{inv}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} m^{inv}$ [pb GeV$^{-1}$]
LogX=1
# END PLOT

# BEGIN PLOT /MC_MET/rap_inv
Title=Rapidity $y^{inv}$ of vector sum of all invisibles
XLabel=$y^{inv}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} y^{inv}$ [pb]
# END PLOT

# BEGIN PLOT /MC_MET/pT_promptinv
Title=$p_\mathrm{T}$ of vector sum of all prompt invisibles
XLabel=$p_\mathrm{T}^{pinv}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_\mathrm{T}^{pinv}$ [pb GeV$^{-1}$]
LogX=1
# END PLOT

# BEGIN PLOT /MC_MET/mass_promptinv
Title=Mass $m^{pinv}$ of vector sum of all prompt invisibles
XLabel=$m^{pinv}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} m^{pinv}$ [pb GeV$^{-1}$]
LogX=1
# END PLOT

# BEGIN PLOT /MC_MET/rap_promptinv
Title=Rapidity $y^{pinv}$ of vector sum of all prompt invisibles
XLabel=$y^{pinv}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} y^{pinv}$ [pb]
# END PLOT
