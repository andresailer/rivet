BEGIN PLOT /BABAR_2017_I1336340/d01-x01-y01
Title=$K^0_S\pi^\pm$ mass in $B^\pm\to K^0_S\pi^\pm\pi^0$
XLabel=$m_{K^0_S\pi^\pm}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^\pm}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2017_I1336340/d01-x01-y02
Title=$K^0_S\pi^0$ mass in $B^\pm\to K^0_S\pi^\pm\pi^0$
XLabel=$m_{K^0_S\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2017_I1336340/d01-x01-y03
Title=$\pi^0\pi^\pm$ mass in $B^\pm\to K^0_S\pi^\pm\pi^0$
XLabel=$m_{\pi^0\pi^\pm}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^\pm}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2017_I1336340/d02-x01-y01
Title=$K^0_S\pi^+$ mass in $B^+\to K^0_S\pi^+\pi^0$
XLabel=$m_{K^0_S\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2017_I1336340/d02-x01-y02
Title=$K^0_S\pi^0$ mass in $B^+\to K^0_S\pi^+\pi^0$
XLabel=$m_{K^0_S\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2017_I1336340/d02-x01-y03
Title=$\pi^0\pi^+$ mass in $B^+\to K^0_S\pi^+\pi^0$
XLabel=$m_{\pi^0\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2017_I1336340/d02-x02-y01
Title=$K^0_S\pi^-$ mass in $B^-\to K^0_S\pi^-\pi^0$
XLabel=$m_{K^0_S\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2017_I1336340/d02-x02-y02
Title=$K^0_S\pi^0$ mass in $B^-\to K^0_S\pi^-\pi^0$
XLabel=$m_{K^0_S\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2017_I1336340/d02-x02-y03
Title=$\pi^0\pi^-$ mass in $B^-\to K^0_S\pi^-\pi^0$
XLabel=$m_{\pi^0\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
