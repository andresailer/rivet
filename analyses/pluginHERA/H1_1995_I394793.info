Name: H1_1995_I394793
Year: 1995
Summary: A Study of the Fragmentation of Quarks in ep Collisions at HERA
Experiment: H1
Collider: HERA
InspireID: 394793
Status: VALIDATED HEPDATA
Reentrant: True
Authors:
 - Narmin Rahimova <min00ag0@gmail.com>
 - Hannes Jung <hannes.jung@desy.de>
References:
 - 'Nucl.Phys.B 445 (1995) 3-21'
 - 'DOI:10.1016/0550-3213(95)91599-H'
 - 'arXiv:hep-ex/9505003'
Beams: [[e-, p+],[p+, e-]]
Energies: [[26.7,820],[820, 26.7]]
Description: 'Deep inelastic scattering (DIS) events, selected from 1993 data taken by the H1 experiment at HERA, are studied in the Breit frame of reference. It is shown that certain aspects of the quarks emerging from within the proton in $ep$ interactions are essentially the same as those of quarks pair-created from the vacuum in $e^+e^-$ annihilation.'
ValidationInfo: 'The average multiplicity vrs. $Q^2$ plot is validated by comparison to the histogram generated from Rivet HZTool. Both Rivet analysis and HZtool was run with two different pdf, HERAPDF20 and CTEQ6L1.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: H1:1995cqf
BibTeX: '@article{H1:1995cqf,
    author = "Aid, S. and others",
    collaboration = "H1",
    title = "{A Study of the fragmentation of quarks in e- p collisions at HERA}",
    eprint = "hep-ex/9505003",
    archivePrefix = "arXiv",
    reportNumber = "DESY-95-072",
    doi = "10.1016/0550-3213(95)91599-H",
    journal = "Nucl. Phys. B",
    volume = "445",
    pages = "3--21",
    year = "1995"
}'

