BEGIN PLOT /HRS_1989_I276948/d01-x01-y01
Title=Scaled momentum for $\rho^0$ at 29 GeV
XLabel=$x_p$
YLabel=$s/\beta\mathrm{d}\sigma/\mathrm{d}x_p$ [$\mu\mathrm{b}\mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /HRS_1989_I276948/d02-x01-y01
Title=Scaled momentum for $K^{*0}$ at 29 GeV
XLabel=$x_p$
YLabel=$s/\beta\mathrm{d}\sigma/\mathrm{d}x_p$ [$\mu\mathrm{b}\mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /HRS_1989_I276948/d02-x01-y02
Title=Scaled momentum for $K^{*+}$ at 29 GeV
XLabel=$x_p$
YLabel=$s/\beta\mathrm{d}\sigma/\mathrm{d}x_p$ [$\mu\mathrm{b}\mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /HRS_1989_I276948/d03-x01-y01
Title=Cross section for $\rho^0$ at 29 GeV
XLabel=$\sqrt{s}$
YLabel=$\sigma(\rho^0)$ [$\mathrm{pb}$]
LogY=0
END PLOT
BEGIN PLOT /HRS_1989_I276948/d04-x01-y01
Title=Cross section for $K^{*0}$ at 29 GeV
XLabel=$\sqrt{s}$
YLabel=$\sigma(K^{*0})$ [$\mathrm{pb}$]
LogY=0
END PLOT
BEGIN PLOT /HRS_1989_I276948/d03-x01-y03
Title=Multiplicity for $\rho^0$ at 29 GeV
XLabel=$\sqrt{s}$
YLabel=$N_{\rho^0}$
LogY=0
END PLOT
BEGIN PLOT /HRS_1989_I276948/d04-x01-y03
Title=Multiplicity for $K^{*0}$ at 29 GeV
XLabel=$\sqrt{s}$
YLabel=$N_{K^{*0}}$
LogY=0
END PLOT
