Name: BELLE_2021_I1809180
Year: 2021
Summary: Helicity and decay angles for the decay $\Xi_c(2790)^+\to\Xi_c^{*0}(\to\Xi_c^+\pi^-)\pi^+$'
Experiment: BELLE
Collider: KEKB
InspireID: 1809180
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 103 (2021) 11, L111101
RunInfo: e+e- to hadrons 
Beams: [e+, e-]
Energies: [10.58]
Options:
 - PID=*
Description: 'Measurement of the helicity and decay angles for the decay $\Xi_c(2790)^+\to\Xi_c^{*0}(\to\Xi_c^+\pi^-)\pi^+$.
The PDG code for this particle is not specified and which multiplet it belongs to is unclear. The default
is to use 204232, i.e. it is in the same multiplet as the Roper resonance, but this can be changed using the PID option.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Belle:2020tom
BibTeX: '@article{Belle:2020tom,
    author = "Moon, T. J. and others",
    collaboration = "Belle",
    title = "{First determination of the spin and parity of the charmed-strange baryon $\Xi_{c}(2970)^+$}",
    eprint = "2007.14700",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "BELLE Preprint 2020-09",
    doi = "10.1103/PhysRevD.103.L111101",
    journal = "Phys. Rev. D",
    volume = "103",
    number = "11",
    pages = "L111101",
    year = "2021"
}
'
