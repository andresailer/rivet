Name: BELLE_2016_I1408873
Year: 2016
Summary: $\chi_{c1}$ and $\chi_{c2}$ production in $B$ decays
Experiment: BELLE
Collider: KEKB
InspireID: 1408873
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 -  Peter Richardson <Peter.Richardson@durham.ac.uk>
References:
 - Phys.Rev.D 93 (2016) 5, 052016
RunInfo:
  $e^+ e^-$ at the $\Upsilon(4S)$
Beams: [e+, e-]
Energies: [10.58]
Description:
  'Measurement of inclusve production of $\chi_{c1}$ and $\chi_{c2}$ in $B$ decays, in reality
   the spectrum at the $\Upsilon(4S)$, together with mass distributions in several $B$ decays.'
ValidationInfo:
  'Herwig 7 events using EvtGen for decays.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Belle:2015opn
BibTeX: '@article{Belle:2015opn,
    author = "Bhardwaj, V. and others",
    collaboration = "Belle",
    title = "{Inclusive and exclusive measurements of $B$ decays to $\chi_{c1}$ and $\chi_{c2}$ at Belle}",
    eprint = "1512.02672",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "BELLE-PREPRINT-2015-21, KEK-PREPRINT-2015-56-(BELLE-COLLABORATION)",
    doi = "10.1103/PhysRevD.93.052016",
    journal = "Phys. Rev. D",
    volume = "93",
    number = "5",
    pages = "052016",
    year = "2016"
}
'
