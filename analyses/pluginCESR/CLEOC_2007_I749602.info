Name: CLEOC_2007_I749602
Year: 2007
Summary: Dalitz plot analysis of $D^+\to \pi^+\pi^+\pi^-$
Experiment: CLEOC
Collider: CESR
InspireID: 749602
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 76 (2007) 012001
RunInfo: Any process producing D+ -> pi+ pi+ pi-
Description:
  'Measurement of the mass distributions in the decay $D^+\to \pi^+\pi^+\pi^-$.
   The data were read from the plots in the paper and the backgrounds subtracted.
   Resolution/acceptance effects have been not unfolded and given the agreement with the model in the paper this analysis should only be used for qualitative studies.'
ValidationInfo:
  'Herwig 7 events using model from paper'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CLEO:2007ktn
BibTeX: '@article{CLEO:2007ktn,
    author = "Bonvicini, G. and others",
    collaboration = "CLEO",
    title = "{Dalitz plot analysis of the D+ ---\ensuremath{>} pi- pi+ pi+ decay}",
    eprint = "0704.3954",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CLNS-07-1993, CLEO-07-3",
    doi = "10.1103/PhysRevD.76.012001",
    journal = "Phys. Rev. D",
    volume = "76",
    pages = "012001",
    year = "2007"
}
'
