BEGIN PLOT /LHCB_2018_I1704426/d01-x01-y01
Title=$K^+K^-$ mass distribution in $D^0\to K^+K^-\pi^+\pi^-$
XLabel=$m_{K^+K^-}$ [$\text{MeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-}$ [$\text{MeV}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2018_I1704426/d01-x01-y02
Title=$\pi^+\pi^-$ mass distribution in $D^0\to K^+K^-\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\text{MeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{MeV}$]
LogY=0
END PLOT
